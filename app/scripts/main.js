var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();

var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://dragonquest.at/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {

            }
        });
    }

    Mediator.subscribe('inputGiven', send);
})();

window.requestAnimFrame = (function(){
	  return  window.requestAnimationFrame       ||
	          window.webkitRequestAnimationFrame ||
	          window.mozRequestAnimationFrame    ||
	          function( callback ){
	            window.setTimeout(callback, 1000 / 60);
	          };
	})();

var animEndEventNames = {
	    'WebkitAnimation' : 'webkitAnimationEnd',
	    'OAnimation' : 'oAnimationEnd',
	    'msAnimation' : 'MSAnimationEnd',
	    'animation' : 'animationend'
};

var data;

function addStartEvent() {
	$('html').on('touchstart', function () {
	    Mediator.publish('inputGiven');
	    unbindEvents();
	});
	$('html').on('mousedown', function () {
	    Mediator.publish('inputGiven');
	    unbindEvents();
	});
	$('html').on('keydown', function () {
	    Mediator.publish('inputGiven');
	    unbindEvents();
	});
}

function unbindEvents() {
	$("html").unbind("touchstart");
	$("html").unbind("mousedown");
	$("html").unbind("keydown");
}

function getCoins() {
	return parseInt($('#coins').html());
}

function startAnimation() {
	requestAnimFrame(function() {
		$('#slot1').removeClass('animation');
		$('#slot2').removeClass('animation');
		$('#slot3').removeClass('animation');
		requestAnimFrame(function() {
			$('#slot1').addClass('animation');
			$('#slot2').addClass('animation');
			$('#slot3').addClass('animation');
		});
	});
}

var setup = (function () {
	$('#running')[0].addEventListener("ended",function() {
	    this.play();
	});
	addStartEvent();
	Mediator.subscribe('datareceived', function (receivedData) {
		$('#running')[0].play();
		$coins = getCoins();
		$('#coins').html($coins - 1);
		startAnimation();
		data = receivedData;
	});
	for ($i = 1; $i <= 3; $i++) {
		var e = document.getElementById("slot" + $i);
		animEndEventName = animEndEventNames[ Modernizr.prefixed('animation') ];
		console.log(animEndEventNames);
		e.addEventListener(animEndEventName, listener, false);
	}
})();

function increaseCoins($addCoins) {
	$coins = getCoins();
	if ($addCoins > 0) {
		$('#win')[0].play();
	}
	else if ($coins == 0) {
		$('#gameOver')[0].play();
	}
	else {
		$('#lose')[0].play();
	}
	$('#coins').html($coins + $addCoins);
	if (getCoins() > 0) {
		addStartEvent();
	}
}

function listener(e) {
	animEndEventName = animEndEventNames[ Modernizr.prefixed('animation') ];
	if (e.type == animEndEventName) {
	  	$element = $(e.target);
	  	$i = $element.attr('id').charAt(4);
	  	$element.removeClass(); // remove all classes
		switch(data[0].slots[$i - 1]) {
			case 0:
				$element.addClass('state-cherry');
				break;
			case 1:
				$element.addClass('state-enemy');
				break;
			case 2:
				$element.addClass('state-vegetable');
				break;
			case 3:
				$element.addClass('state-star');
				break;
		}
		if ($i == 3) {
			$('#running')[0].pause();
			increaseCoins(data[0].result);
		}	
	}
}


